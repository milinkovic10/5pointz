import Vue from 'vue'
import App from './App.vue'
import VueBus from 'vue-bus';
import 'fullpage.js/vendors/scrolloverflow' // Optional. When using scrollOverflow:true
import VueFullPage from 'vue-fullpage.js';

Vue.use(VueFullPage)

Vue.use(VueBus);
Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app');
